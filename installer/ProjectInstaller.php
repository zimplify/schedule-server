<?php
    namespace Zimplify\Installer;
    use Composer\Script\Event;

    /**
     * this installer help to establish all the key 
     * Package: 2 (Scaffold for applications)
     * Type: 1 (Installer)
     * Item: 001 (Installer)
     */
    final class ProjectInstaller {

        const FILE_COMPOSER = "composer.json";
        const FILE_REPLACE = "replace.json";
        const CMD_CRON = "(crontab -l 2>/dev/null; echo \"*/0,15,30,45 * * * * /opt/<path>/scheduler/system/schedule.sh -with args\") | crontab -";
        

        /**
         * this is the main trigger function for us to start the installer change
         * @param Event $event the composer event from create-project
         * @return void 
         */
        public static function run(Event $event) {

            // need to move the composer from replace
            if (file_exists(self::FILE_REPLACE) && file_exists(self::FILE_COMPOSER)) 
                rename(self::FILE_REPLACE, self::FILE_COMPOSER);

            // we need to inject to cron
            if (!is_null($path = getenv("SCRIPT_DIR"))) {                
                $command = str_replace(self::SUB_PATH, $path, self::CMD_CRON);
                exec ("$command;");
            }
            
        }
    }