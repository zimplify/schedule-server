<?php
    namespace Zimplify\Installer;
    use Composer\Composer;
    use Composer\Script\Event;
    use Composer\Installer\PackageEvent;
    use \RuntimeException;

    /**
     * @package Zimplify\Installers (code 8)
     * @type installer (code 6)
     * @file ModuleInstaller (code 02)
     */
    final class ModuleInstaller {

        const ARGS_NAMESPACE = "namespace";
        const DEF_ROUTE_LOCATION = self::FILE_ROUTES;
        const DIR_LIB = "./lib";
        const DIR_MODUILES = "./app/routes";
        const FILE_COMPOSER = "./composer.json";
        const FILE_DOC_CLASS = "./lib/app/config/app.documents.config";
        const FILE_MODULES = "./lib/app/config/app.modules.config";
        const FILE_NAMESPACES = "./.namespaces";
        const FILE_PACKAGE = "package.json";
        const FILE_PARAMS = "./lib/app/config/app.operation.config";
        const FILE_ROUTES = "routes.php";
        const FILE_SCHEDULER = "./scheduler.php";
        const FILE_STORAGE = "./lib/app/config/app.storage.config";        
        const FILE_TASKS = "./.tasks";
        const FLD_CONTAINERS = "containers";
        const FLD_ENVS = "envs";
        const FLD_MAPS = "maps";
        const FLD_PACKAGE_NAME = "name";
        const FLD_ROUTES = "routes";
        const FLD_SCRIPTS = "scripts";
        const FLD_SCHEDULES = "schedules";
        const PACKAGE_SCHEDULER = "zimplify\scheduler";

        static $tasks = [];

        /**
         * creating new parameters into installation file
         * @param string $path the name of the package
         * @param array $updates the incoming data
         * @return void
         */
        protected static function assign(string $path, array $updates) {
            if (file_exists(self::FILE_PARAMS) && count($updates) > 0) {

                // only update environment if we have a need to
                if (!is_null($current = json_decode(file_get_contents(self::FILE_PARAMS), true)) &&
                    array_key_exists(self::FLD_ENVS, $updates)) {
                    error_log("MOD-INSTALL> loading updates");
                    $future = static::update($current, $updates[self::FLD_ENVS]);
                    file_put_contents(self::FILE_PARAMS, json_encode($future));
                }
            } else 
                throw new RuntimeException("Required file for environment updates is missing");
        }

        /**
         * loading all script elements ordered. 
         * @param string $path the path of the package
         * @param array $setup the array of incoming data
         * @return void
         */
        protected static function configure(string $path, array $setup) {
            if (array_key_exists(self::FLD_SCRIPTS, $setup))
                foreach ($setup[self::FLD_SCRIPTS] as $script) 
                    exec(str_replace("<h>", $path, $script));
        }

        /**
         * searchinng for a package and report if it exists
         * @param string $package the name of the package to look for
         * @return bool
         */
        private static function isLoaded(Composer $composer, string $package) : bool {
            // now get our repository first
            $repository = $composer->getRepositoryManager();

            // now we look for our package
            $package = $repository->findPackage($package);

            return !is_null($package) ? true : false;
        }        
        
        /**
         * create symbolic link to library to add the section
         * @param string $path the path of the package
         */
        protected static function link(string $path, string $name) {
            if (is_dir(self::DIR_LIB)) symlink($path."/lib", self::DIR_LIB."/$name");              
        }
        
        /**
         * adding the mapping information needed to get working
         * @param string $path the package path
         * @param array $updates the updated data array from package.json
         * @return void
         */
        protected static function map(string $path, array $updates) {
            // error_log("LOG: \$path is $path");
            // error_log("LOCAL: ".json_encode(file_exists(self::FILE_MODULES)));
            // error_log("REMOTE: ".json_encode(file_exists($path."/".self::FILE_PACKAGE)));
            if (file_exists(self::FILE_MODULES) && file_exists($path."/".self::FILE_PACKAGE)) {
                $old = file_get_contents(self::FILE_MODULES);

                // now getting our configs
                $current = json_decode($old, true);
                $updated = false;
                $package = array_key_exists(self::FLD_PACKAGE_NAME, $updates) ? $updates[self::FLD_PACKAGE_NAME] : null;

                // check if there is a map update
                if (array_key_exists(self::FLD_MAPS, $updates) && $package) {
                    $current[$package] = $updates[self::FLD_MAPS];
                    file_put_contents(self::FILE_MODULES, json_encode($current));   
                    
                    // added for CR-ZIM-040
                    if (file_exists(self::FILE_NAMESPACES)) {
                        $namespaces = json_decode(file_get_contents(self::FILE_NAMESPACES), true) ?? [];    
                    } else
                        $namespaces = [];                    
                    if (!is_null($incoming = $updates[self::FLD_MAPS][self::ARGS_NAMESPACE])) 
                        if (!in_array($incoming, $namespaces))
                            array_push($namespaces, $incoming);
                    file_put_contents(self::FILE_NAMESPACES, json_encode($namespaces));
                }
            } else 
                throw new RuntimeException("Required file for module mapping is missing");
        }

        /**
         * write the tasks to disk before firing the adding script
         * @oaram Event $event the composer triggered event
         * @return void
         */
        public static function generate(Event $event) {
            file_put_contents(self::FILE_TASKS, json_encode(static::$tasks));
        }
        
        /**
         * the main installation function
         * @param Composer $composer the composer instance currently running
         * @return void
         */
        public static function install(PackageEvent $event) {
            // loading up the installer
        
            $installation = $event->getComposer()->getInstallationManager();
            $package = $event->getOperation()->getPackage();
            $path = $installation->getInstallPath($package);

            // check the package
            if (file_exists($path."/".self::FILE_PACKAGE)) {

                // reading out the package file
                $setup = file_get_contents($path."/".self::FILE_PACKAGE);
                $setup = json_decode($setup, true);

                // linking to the package at lib.
                if (array_key_exists(self::FLD_PACKAGE_NAME, $setup)) { 
                    
                    static::link($path, $setup[self::FLD_PACKAGE_NAME]);
                    
                    // copy files into modules file
                    static::map($path, $setup);

                    // copy files into package operation file
                    static::assign($path, $setup);
                    
                    // linking the package so we can install routes
                    static::reroute($path, $setup[self::FLD_PACKAGE_NAME]);

                    // copying data container defs
                    if (array_key_exists(self::FLD_CONTAINERS, $setup)) static::store($setup[self::FLD_CONTAINERS]);

                    // loading tasks into static array for autoloading
                    if (array_key_exists(self::FLD_SCHEDULES, $setup)) static::setup($setup[self::FLD_SCHEDULES]);

                    // installation scripts that is applicable
                    if (array_key_exists(self::FLD_SCRIPTS, $setup)) static::configure($path, $setup[self::FLD_SCRIPTS]);

                } else 
                    throw new RuntimeException("Improper package details.", 500);
            }
        }

        /**
         * assign the module route file into system
         * @param string $path the composer package installed path
         * @param string $package the name of the package directory to read from (see package name)
         * @return void
         */
        protected static function reroute(string $path, string $package) {
            // only run this if the route file is there...
            if (file_exists($path."/".self::DEF_ROUTE_LOCATION) && is_dir(self::DIR_MODUILES)) {

                // create the real link
                symlink($path."/".self::DEF_ROUTE_LOCATION, self::DIR_MODUILES."/$package-route.php");

            } else 
                error_log("INSTALLER: unable to locate directory ".self::DIR_MODUILES);
        }

        /**
         * setting tasks to be scheduled into system
         * @param array $tasks the list of classes that linked to task
         * @return void
         */
        public static function setup(array $tasks) : void {
            static::$tasks = array_merge(static::$tasks, $tasks);
        }

        /**
         * managing containers we need to record
         * @param array $containers the incoming containers
         * @return void
         */
        protected static function store(array $containers = []) : void {
            $storage = file_exists(self::FILE_STORAGE) ? (json_decode(file_get_contents(self::FILE_STORAGE) , true) ?? []) : [];
            foreach ($containers as $container => $setup) 
                if (!array_key_exists($container, $storage)) 
                    $storage[$container] = $setup;
            file_put_contents(self::FILE_STORAGE, json_encode($storage));
        }

        /**
         * all the uninstall logic canned when pre-package-uninstall
         * 1. remove the lib link
         * 
         */
        public static function uninstall() {
            // we need to add the uninstalll logic right here.
            // remove module related things
            // should we remove all properties on file??
        }

        /**
         * updating the package parameters 
         * @param array $source the source data array
         * @param array $values the new data source array
         */
        protected static function update(array $source, array $values) : array {
            $result = $source;
            foreach ($values as $f => $v) {
                if (array_key_exists($f, $source)) 
                    if (is_array($v)) 
                        $result[$f] = static::update($source[$f], $v);
                else 
                    $result[$f] = $v;
            }
            return $result;
        }
    }