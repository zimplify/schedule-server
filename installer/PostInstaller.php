<?php
    namespace Zimplify\Installer;
    use Zimplify\Core\{Application, Document, Query};
    use Zimplify\Core\Services\StorageUtils;
    use HaydenPierce\ClassFinder\ClassFinder;

    /**
     * the PostInstaller works on what to do after the autoloader dumping
     * @package Zimplify\Installer (code 08)
     * @type Installer (code 14)
     * @file PostInstaller (code 03)
     */
    class PostInstaller {

        const FILE_DOC_CLASS = "./lib/app/config/app.documents.config";
        const FILE_STORAGE = "./lib/app/config/app.storage.config";
        const FILE_NAMESPACES = "./.namespaces";
        const FILE_TASKS = "./.tasks";

        /**
         * work to generate document index
         * @return void
         */
        protected static function index() : void {
            if (file_exists(self::FILE_NAMESPACES)) {
                $ns = json_decode(file_get_contents(self::FILE_NAMESPACES), true);
                
                // we need to add the additional namespace 
                $apps = json_decode(Application::fetch("modules", "app", "config"), true);
                array_push($ns, $apps["app"]["namespace"]);

                // now run the real things
                $documents = [];
                foreach ($ns ?? [] as $namespace) {
                    $cls = ClassFinder::getClassesInNamespace($namespace."\\Requests");
                    foreach ($cls ?? [] as $class)                         
                        if (is_a($class, "Zimplify\\Core\\Interfaces\\IWorkflowCapableInterface", true)) 
                            array_push($documents, $class);                    
                }

                // now do the writing
                file_put_contents(self::FILE_DOC_CLASS, json_encode($documents));
                unlink(self::FILE_NAMESPACES);
            }
        }

        /**
         * this is our main function to operate
         * @return void
         */
        public static function run() {
            static::index();            // create the master document index
            static::schedule();         // hooking up the tasks
        }

        /**
         * task to ensure all tasks are recorded
         * @return void
         */
        protected static function schedule() : void {
            if (file_exists(self::FILE_TASKS)) {
                foreach (json_decode(file_get_contents(self::FILE_TASKS), true) ?? [] as $task) 
                    if (!is_null($name = constant($task."::DEF_SHT_NAME"))) 
                        if (count(Application::search([Query::SRF_TYPE => $name])) == 0) 
                            Application::create($name)->save();
                // unlink(self::FILE_TASKS);
            }
        }

    }