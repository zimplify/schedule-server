<?php
    use Zimplify\Core\{Application, Query};
    use \RuntimeException;

    require_once "./vendor/autoload.php";
    
    try {
        if ($argc != 2)
            if ($argv[1] === "-h") {
                print("USAGE: php trigger.php <task id>");
                exit(0);
            } else {
                $task = Application::search([Query::SRF_ID => $argv[1]], "tassk");
                if (count($task) == 1) {
                    $task->run();
                    exit(0);
                } else 
                    throw new RuntimeException("Unable to locate tasks.");
            }            
        else 
            throw new RuntimeException("Invalid number of arguments");
    } catch (Exception $ex) {
        exit(-1);
    }
    