#!/bin/sh

# need to change to the server directory to inherit all paths
cd /opt/$SCRIPT_DIR/scheduler

# run the scheduler
php ./system/manager.php

# now exit
exit 1;
