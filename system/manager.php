<?php
    use Zimplify\Scheduler\Scheduler;

    require_once "./vendor/autoload.php";

    // trigger our scheduler - __invoke will trigger
    new Scheduler();

    // just exit when your done.
    exit(1);
